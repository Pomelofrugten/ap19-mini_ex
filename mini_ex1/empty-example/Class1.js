//var for x placement, used with random
var x=-200;
// var for color (test)
var col={
  r:238,
  g:232,
  b:170
};
// var for blinking star, used with random
var col2={
  r:238,
  g:232,
  b:170
};
// var for flying star, used with random.
// want to make it go slower. maybe something with framecount?
var punkt={
  x:300,
  y:50
};

function setup() {
  // WEBGL used when 3D
  createCanvas(windowWidth,windowHeight, WEBGL)
  //removed cursor for artistic look :)
  noCursor();
}

function draw() {
  background(color(0,0,100));
  // making the objects change randomly with every frame
  punkt.x=random(-300,width);
  punkt.y=random(-300,height);
  col2.r=random(100,255);
  col2.g=random(100,255);
  col2.b=random(100,255);

  //moves 1 on the x-axis with every frame
  x=x+1;
  //in desperate need of a variable that places more objects at the same time
  fill(col.r,col.g,col.b);
  circle(punkt.x,punkt.y,3);
  fill(col.r,col.g,col.b);
  // floating star
  circle(x,200,3);
  fill(col.r,col.g,col.b);
  circle(300,300,3);
  fill(col.r,col.g,col.b);
  circle(100,300,3);
  fill(col.r,col.g,col.b);
  circle(115,200,3);
  fill(col.r,col.g,col.b);
  circle(-200,0,3);
  fill(col.r,col.g,col.b);
  circle(-400,10,3);
  fill(col.r,col.g,col.b);
  circle(-500,-10,3);
  fill(col.r,col.g,col.b);
  circle(-300,-40,3);
  fill(col.r,col.g,col.b);
  circle(-200,-100,3);
  fill(col.r,col.g,col.b);
  circle(-700,-100,3);
  fill(col.r,col.g,col.b);
  circle(-400,-215,3);
  // color changing with random
  fill(col2.r,col2.g,col2.b);
  circle(200,-150,3);
  fill(col2.r,col.g,col.b);
  circle(400,0,3);
  fill(col.r,col.g,col.b);
  circle(300,12,3);
  fill(col.r,col.g,col.b);
  circle(500,70,3);

  //using the unfilled beziers for creating wavey milkeyway
  noFill();
  //stroke = color of line
  stroke(250);
  bezier(-750,200,-400,1000,0,300,1000,-100);
  noFill();
  stroke(250);
  bezier(750,-200,400,-1000,0,-300,-1000,100);

  fill(80,0,100);
  sphere(100,200,200);

  //rotating the torus around the planet
  fill(100,149,257,170);
  rotateY(3 / 3.0);
  rotateX(frameCount*0.01);
  torus(200,15,200,200);
}
