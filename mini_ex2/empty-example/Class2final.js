/*Cecilie Christensen mini_ex2
spørg om hjælp til constrain()
for at kunne tilføje to mouseDragged*/

let outbrow=250;
let upbrow=1;
let mundvige=450;
/*let bceil=230;
let bfloor=300;
let browdrag=constrain(mouseY,bceil,bfloor);*/

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(0,0,139);
}

function draw(){
  //boxes to cover text
  fill(0,0,139);
  noStroke();
  rect(0,0,width,100);
  rect(0,600,width,200)

  //head
  noStroke();
  fill(255,255,0);
  ellipse(670,350,400,600);
    //left eyebrow
    stroke(0);
    strokeWeight(8);
    bezier(540,outbrow,560,250,580,250,600,outbrow);
    //right eyebrow
    bezier(740,outbrow,760,250,780,250,800,outbrow);
    //left eye
    strokeWeight(1);
    fill(255);
    ellipse(565,300,40,40);
    fill(0);
    ellipse(565,300,20,20);
    //right eye
    fill(255);
    ellipse(765,300,40,40);
    fill(0);
    ellipse(765,300,20,20);
    //mouth
    noFill();
    strokeWeight(10);
    bezier(540,mundvige,600,450,740,450,800,mundvige);

    //change mouth position
    if(keyIsDown(DOWN_ARROW)){
      mundvige+=1;
    }
    if(keyIsDown(UP_ARROW)){
      mundvige-=1;
    }

    //text change
    if(mouseY>=300){
      textSize(32);
      noStroke();
      fill(255,192,203);
      text('Drag your eyebrows up and down',425, 30);
      text('Smile and frown with up and down arrow',370,700);
    } else{
      textSize(32);
      noStroke();
      fill(65,105,225);
      text('How are you today?',540, 30);
      text('?',660,700);
    }
  }

//change eyebrows
function mouseDragged(){
  outbrow=outbrow+upbrow;
  if(mouseY<300){
    upbrow=-1;
  }
  if(mouseY>300){
    upbrow=1;
  }
}
