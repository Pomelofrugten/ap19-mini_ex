/*lav den manipulativ*/
let smile
let frown
let brows
let brows2
let brows3
let brows4

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(0,0,139);
}

function draw(){

  //right box to cover text
  fill(0,0,139);
  noStroke();
  rect(300,0,700,100);

  //head
  noStroke();
  fill(255,255,0);
  ellipse(670,350,400,600);
    //mouth
    noFill();
    stroke(0);
    strokeWeight(10);
    bezier(550,400,659,smile,660,smile,770,frown);
    //left eye
    strokeWeight(1);
    fill(255);
    ellipse(610,280,40,40);
    fill(0);
    ellipse(610,280,20,20);
    //right eye
    strokeWeight(1);
    fill(255);
    ellipse(720,280,40,40);
    fill(0);
    ellipse(720,280,20,20);
    //left eyebrow
    noFill();
    strokeWeight(5);
    bezier(580, brows, 610, brows, 620, brows2, 640, brows3);
    //right eyebrow
    bezier(690, brows3, 734, brows2, 735, brows, 750, brows);

    if(mouseY>=300){
      //lower eyebrows, smile
      textSize(32);
      noStroke();
      fill(255,192,203);
      text('Akward but happy', 550, 30);
      brows=240;
      brows2=225;
      brows3=235;
      smile=445;
      frown=400;
    } else{
      //lift eyebrows, frown
      textSize(32);
      noStroke();
      fill(65,105,225);
      text('Doubting the world', 540, 30);
      brows=255;
      brows2=250;
      brows2b=240;
      smile=400;
      frown=340;
    }
  }
