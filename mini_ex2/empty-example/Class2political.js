let smelt=100;
let nedad=305;
let stp1=150;
let stp2=170;
let sstp=160;
let tear=122;

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(70);
}

function draw(){
  //blood
  noStroke();
  fill(240,0,0);
  ellipse(mouseX,mouseY, 10,10);

  noStroke();
  fill(70);
  rect(0,0,1000,1000);

  //Whale
  noStroke();
  fill(200);
  rect(200,100,70,70,20,50,50,20);
  triangle(243,100,360,150,243,170);
  rect(350,130,10,40);
  stroke(0);
  strokeWeight(2);
  line(220,120,225,125);
  line(225,120,220,125);
  stroke(200);
  strokeWeight(6);
  line(265,160,210,180);
  noStroke();

  //tear
  if(tear>350){
    tear=122;
  }
  fill(0,0,255);
  ellipse(222, tear, 5, 8);

  //iceberg
  strokeWeight(2);
  fill(255);
  triangle(500, nedad, 600, smelt, 700, nedad);
  stroke(200);
  line(575,stp1,595,stp2);
  line(610,stp1,595,stp2);
  line(610,stp1,625,stp2);
  line(630,sstp,625,stp2);

  //endings
  noStroke();
  fill(70);
  rect(0,350,1000,1000);

  //ocean
  fill(0,0,255,100);
  ellipse(700,360,1300,100);

  //melt iceberg
  if(keyIsDown(DOWN_ARROW)){
    nedad+=1;
    smelt+=1;
    stp1+=1;
    stp2+=1;
    sstp+=1;
    tear+=1;
  }
  if(keyIsDown(UP_ARROW)){
    nedad-=1;
    smelt-=1;
    stp1-=1;
    stp2-=1;
    sstp-=1;
  }

  textSize(32);
  fill(255,0,0);
  text('press down arrow',600,600);



  // for( var i=0; i<228; i++){
  //   tear+=1
  // }
}

function mousePressed(){
  console.log(mouseX,mouseY);
}
