![screenshot](mini_ex2.png)

**How are you Today?**

[Link til program](https://gl.githack.com/Pomelofrugten/ap19-mini_ex/raw/master/mini_ex2/empty-example/index2.0.html)  

**Inspiration**  
I was inspired by Mogens Jacobsen's art piece ['The Pill Machine'](https://www.youtube.com/watch?v=RRA8aWveSo8)  
and Glenn Christians book 'Månen Atlas over Sandparken Avenue' (The moon Atlas over the Sandpark Avenue, 2018):  
>*"Fjer blev  
Som de fleste  
Som de mange  
Ældre  
Voksede i flere retninger  
Et kuglerundt   
ansigt  
Overalt*

>*Mig  
Om hvem jeg intet ved  
"*  

The visual design of my program also mimics [Lea Porsagers work](https://www.google.com/search?q=glenn+christian+m%C3%A5nen+atlas&source=lnms&tbm=isch&sa=X&ved=0ahUKEwj5u6mcqLngAhXGPFAKHdnqD14Q_AUIDigB&biw=1440&bih=714#imgrc=Q-8A735ShHe4JM:) for Glenn Christians book cover, which already has some emoji connotations:

**Relation to Modifying the Universal**  
Both artworks are absurd imitations of human beings.  
There are some similarities with real humans, but none of them captures an entire human form.  
Mogens Jacobsen "forces" the person into a smiley face, and Glenn's writing is very staccato and his characters are confusing.  
They both get humans to resemble machines.  
I wanted to bring the human more into the machine, and my first thought was that the only way to do this was to restrict the portrayal to a single quality:  
In my case emotions.  

When I read Modifying the Universal I really started thinking about the cultural and political consequences of trying to be more embracing.  
Because to be able to embrace everyone is, in my opinion, impossible.  
I think that Unicode and the companies using emojis are a need for the user to express oneself, that  
- a) cannot be done with a single emoticon  
- and b) is unnecessary for the user.  

I think the use of emojis originates from wanting (or needing) to express emotions in texting.  
This wish has now become an expression of the entire self.  
My program is, therefore, an expression of emotions rather than of personality.  
I want it to be viewed as something impulsive, drifting instead of something defining.  
I wanted to make a program that worked as a comment on the current emojis on my iPhone.  

**The code**  
Codewise I made an oval smiley face with an ellipse as head, ellipses as eyes and beziers for the mouth and eyebrows.  
I used conditional statements to be able to switch between different states.  
This way my emoji holds a lot of different emojis.  
By manipulating the mouth and eyebrows you get different emotions in its face.  
You can manipulate them a little or a lot; make them both turn downwards or the mouth up and the eyebrows down and so on.  
This way different emotions - and different emojis - come out.  

*Earlier drafts*  
![screenshot](død.png)  
https://gl.githack.com/Pomelofrugten/ap19-mini_ex/raw/master/mini_ex2/empty-example/index666.html  
https://gl.githack.com/Pomelofrugten/ap19-mini_ex/raw/master/mini_ex2/empty-example/index1.html  
https://gl.githack.com/Pomelofrugten/ap19-mini_ex/raw/master/mini_ex2/empty-example/indexdraft.html

*literature:*  
Christian, G., "The Moon Atlas over the Sandparken Avenue", Gyldendal, 2018  
Abbing, R.R, Pierrot, P and Snelting, F., "Modifying the Universal." Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver. Open Humanities Press, 2018, pp. 35-51  