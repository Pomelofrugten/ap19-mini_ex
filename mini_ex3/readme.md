**Throbber**  
https://gl.githack.com/Pomelofrugten/ap19-mini_ex/raw/master/mini_ex3/empty-example/index3.html  

I have made a quite simple throbber, that is inspired by the one we know from iPhone shutting down or [Netflix loading](https://www.google.com/search?biw=1302&bih=713&tbm=isch&sa=1&ei=D8hzXNKgC4e5kwW_56TADA&q=netflix+loading&oq=netflix+loading&gs_l=img.3..0i19l10.3678.4539..4625...0.0..0.81.509.7......1....1..gws-wiz-img.......0j0i30.MRkN9mfE984#imgrc=QAKLml6UiFUcWM:).  
I wanted it to be more square than round mostly because it seemed easier to make, but also because it would then get a more static vibe, that reminds me of waiting.  
I have worked with the frustration that a throbber gives me in multiple ways:
- By making the lines of dots move out and become transparent, you think that your page is loaded.
- If you wait long enough the transparency and the movement of the rect-dots will get out of sync. (This was not on purpose, but it really frustrated me, which is a good thing in this case)
- I was a little behind with the tutorial videos when I started making my throbber, which also made me frustrated. Coding was suddenly very hard!! :((((. So even though I continued looking into animation possibilities, I kept this version of my throbber to give my piece a meta-frustration.  

In the text 'Throbber: Executing Micro-temporal Streams' Winnie Soon writes: *"In contrast to a progress bar, which is more linear in form, a throbber does not indicate any completed or finished status and progress. It does not explain any processual tasks in any specific detail."*  
It is this uncertainty and lack of explanation that I try to investigate with my throbber.

The syntax I used is simply the rect, and then I tried the for loops and if statements.  
My lines move outwards, and I hid them with black rectangles because I did not know how else to do it.  
Find my code in empty-example/Class3.js

![Screenshot](Throbber1.png)
![Screenshot](Throbber2.png)  
![Screenshot](Throbberr3.png)
![Screenshot](Throbber4.png)